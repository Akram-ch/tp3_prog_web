<?php
//include 'LOTR.html';
require_once "tp3-helpers.php";
require_once "get_info.php";
echo "<style type='text/css'>
table {
    width : 100%;
}
img {
    height : 350px;
    width : 250px;
    display : block;
    margin-right : auto;
    margin-left : auto;
}
 tr:hover {background-color: DarkViolet;} 
</style>";
$cast = array();
$url;
$url_prefix = "http://api.themoviedb.org/3/search/movie?api_key=";
$api_key = "ebb02613ce5a2ae58fde00f4db95a9c1";
$url = $url_prefix .$api_key . "&query=" . "Lord+Of+The+Rings";
list($content, $info) = smartcurl($url);
$jason = json_decode($content, true);
$jason_r = $jason['results'];
for ($i = 0; $i < 3 ; $i++) {
    $movie = $jason_r[$i]['id'];
    $info_og = get_info($movie,NULL);
    $info_en = get_info($movie,array("language"=>"en"));
    $info_fr = get_info($movie,array("language"=>"fr"));
    list($credits,$info) = smartcurl("http://api.themoviedb.org/3/movie/".$movie ."/credits?api_key=ebb02613ce5a2ae58fde00f4db95a9c1");
    $credits = json_decode($credits,true);
    $credits = $credits["cast"];
    foreach ($credits as $artist){
        if ($artist["known_for_department"] == "Acting"){
            if (array_key_exists($artist["original_name"],$cast) == FALSE){
                $id = $artist["id"];
                $cast[$artist["original_name"]] = [
                    "name" =>"<a href=\"https://www.themoviedb.org/person/".$id."\">".$artist["original_name"] ."</a>",
                    "role" => $artist["character"],
                    "appearances" => 1
                ];
            }
            else{
                $cast[$artist["original_name"]]["appearances"]++;
            }
        }
    }




    echo "<table border = 1px solid black>";
    echo "
    <thead>
        <tr>
            <th colspan = \"3\" > Détails Sur le Film </th> 
        </tr>
        <tr>
            <th> Version Originale </th> 
            <th> Version Anglaise </th> 
            <th> Version Française </th>
        </tr>
    </thead>
    ";
    echo "
    <tbody>
    ";
    foreach ($info_og as $key => $value){
        $ang = $info_en[$key];
        $fra = $info_fr[$key];
        echo "<tr>
            <td>$value</td>
            <td>$ang</td>
            <td>$fra</td>
        </tr>
        ";
    }
    echo "</tbody>";
    echo "</table>";
    
    echo "<br>
    <br>
    <br>";

    echo "<br>";
    echo "<br>";
    echo "<br>";
    echo "<br>";

    $poster_link = "https://image.tmdb.org/t/p/w500" . $info_og['poster'];
    echo "<image src=\"$poster_link\" >";
    echo "<br><br><br><br>
    
    ";



}

echo "<table border= 1px solid black>";
echo "<thead>
        <tr>
            <th colspan=\"3\">Movie Cast </th>
        </tr>
        <tr>
            <th> Actor </th>
            <th> Role </th>
            <th> Appearances </th>
        </tr>
    </thead> ";

foreach ($cast as $key => $value){
    $name = $value['name'];
    $role = $value['role'];
    $appearances = $value['appearances'];
    echo "
    <tr>
        <td>$name</td>
        <td>$role</td>
        <td>$appearances</td>
    </tr>
    ";
}






echo "</table>";

?>