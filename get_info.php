<?php
require_once "tp3-helpers.php";
function get_info ($urlcomponent,$param = NULL)
{
    //tmdbget($urlcomponent,$param);
    $jason = tmdbget($urlcomponent,$param);
    $jason = json_decode($jason, true);
    //print_r($jason);
    if ($param == NULL){
        $lang =array('language' => $jason['original_language']);
        $jason = tmdbget($urlcomponent,$lang);
        $jason = json_decode($jason, true);
    }
    $presentation = array();
    $id = $jason['id'];
    $title = $jason['title'];
    $og_title = $jason['original_title'];
    $tagline = $jason['tagline'];
    $poster = $jason['poster_path'];
    $description = $jason['overview'];
    $date = $jason['release_date'];




    $presentation["Identifiant"] = $id;
    $presentation["title"] = $title;
    $presentation["original title"] = $og_title;
    $presentation["tagline"] = $tagline;
    $presentation["description"] = $description;
    $presentation["release_date"] = $date;
    $presentation["poster"] = $poster ;
    return $presentation;
}


?>